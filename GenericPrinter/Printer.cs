﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        private const string PRINTER_GROUP_NAME = "GridPrinter";
        private bool _loadSuccess = false;
        
        private ScreenMenu _screenMenu;

        private IMyPistonBase _piston;
        private List<IMyShipWelder> _welders;
        private IMyProjector _projector;
        private IMyShipMergeBlock _mergeBlock;
        private IMyTextPanel _display = null;

        public Program ()
        {
            
        }

        private bool TryLoad ( out string statusMessage )
        {
            var group = GridTerminalSystem.GetBlockGroupWithName ( PRINTER_GROUP_NAME );

            if (group == null)
            {
                statusMessage = $"No group named '{PRINTER_GROUP_NAME}'!";
                return false;
            }

            _screenMenu = new ScreenMenu ( "root" )
            {
                MenuItems = new List<ScreenMenuItem>
                {
                    new ScreenMenuItem ( "Begin Print", BeginPrint ),
                    new ScreenMenuItem ( "Reset", ResetPrinter )
                }
            };



            statusMessage = "Loaded successfully";
            return true;
        }

        private void ResetPrinter ()
        {
            throw new Exception ("Not Implemented");
        }

        private void BeginPrint ()
        {
            throw new Exception ( "Not Implemented" );
        }

        public void Save ()
        {
            // Might need this, might not.
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            if ( !_loadSuccess )
            {
                string status;
                _loadSuccess = TryLoad ( out status );

                Echo ( status );
            }


        }
    }
}
