﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;
using Sandbox.Game.Screens.Terminal.Controls;
using Sandbox.ModAPI.Interfaces.Terminal;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // Change this to whatever the name of you debug LCD is
        const string DEBUG_LCD_NAME = "Debug";
        // Run the command block with namy of the below commands

        private readonly IMyTextSurface _echoSurface = null;
        private IEnumerator<bool> _stateMachine = null;
        public Program ()
        {
            var echoBlock = GridTerminalSystem.GetBlockWithName(DEBUG_LCD_NAME);

            if (echoBlock is IMyTextPanel )
            {
                _echoSurface = (IMyTextPanel) echoBlock;
                _echoSurface.WriteText ( "", false );
                Echo = EchoToLcd;
            }

            _echoSurface?.WriteText ( "", false );

            var blocks = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock> ( blocks );

            _stateMachine = WriteBlocks ( blocks );

            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        private void EchoToLcd (string input)
        {
            _echoSurface.WriteText ( input, true );
        }

        public void Save ()
        {
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            RunStateMachine ();
        }

        private void RunStateMachine ()
        {
            bool hasMoreSteps = _stateMachine != null && _stateMachine.MoveNext ();

            if (!hasMoreSteps)
            {
                _stateMachine.Dispose ();
                _stateMachine = null;
                Runtime.UpdateFrequency = UpdateFrequency.None;
                Me.Enabled = false;
            }
        }
        
        private IEnumerator<bool> WriteBlocks ( List<IMyTerminalBlock> blocks )
        {
            Echo ( "STARTING...\n" );

            foreach ( var block in blocks )
            {
                Echo ( "\n-----------------\n" );
                Echo ( $"Block Name: {block.DisplayNameText}\n" );
                Echo ( $"Block Info: {block.DetailedInfo}\n" );

                var actions = new List<ITerminalAction>();
                block.GetActions ( actions );

                Echo ( "ACTIONS:\n" );

                foreach ( var a in actions )
                {
                    Echo ( $"...ActionID={a.Id}|ActionName={a.Name}\n" );
                }

                var properties = new List<ITerminalProperty>();

                block.GetProperties ( properties );

                foreach ( var prop in properties )
                {
                    Echo ( $"->Id:{prop.Id}|TypeName:{prop.TypeName}" );
                    switch ( prop.TypeName )
                    {
                        case "Boolean":
                            Echo ( $"-->{block.GetValueBool ( prop.Id )}\n" );
                            break;
                        case "StringBuilder":
                            Echo ( $"-->{block.GetValue<StringBuilder> ( prop.Id )}\n" );
                            break;
                        case "Single":
                            Echo ( $"-->{block.GetValueFloat ( prop.Id )}\n" );
                            break;
                        case "Int64":
                            Echo ( $"-->{block.GetValue<long> ( prop.Id )}\n" );
                            break;
                        case "Dictionary`2":
                            HandleDictionary ( prop, block );
                            break;
                        default:
                            Echo ( $"-->Found unknown type: {prop.TypeName}\n" );
                            break;
                    }
                }

                yield return true;
            }
        }

        private void HandleDictionary ( ITerminalProperty prop, IMyTerminalBlock block)
        {
            try
            {
                var dictionary = block.GetValue<Dictionary<string,Delegate>> ( prop.Id );

                Echo ( $"All Keys: [{string.Join(",",dictionary.Keys)}]" );

                foreach ( var key in dictionary.Keys )
                {
                    var del = dictionary[key];
                    object res = null;

                    try
                    {
                        res = del.DynamicInvoke (block);

                        if ( res == null )
                        {
                            res = "NULL";
                        }
                    } catch (Exception ex)
                    {
                        res = ex.Message;
                    }

                    Echo ( $"{key}={res.ToString ()}\n" );
                }
                
            } catch (Exception ex )
            {
                Echo ( ex.GetType ().ToString () );
            }
        }
    }
}
