﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;
using System.Text.RegularExpressions;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        //////////////////////////////////
        ////////// START CONFIG //////////
        //////////////////////////////////

        // How often to scan for new block changes. Higher values are more performance friendly.
        const int GRID_SCAN_INTERVAL_SECONDS = 10;

        // Echo debug info?
        const bool INCLUDE_DEBUG_INFO = false;

        // HP threshhold % to lower charge rate - probably don't adjust this
        const float HIGH_HP_PERCENT = 98;
        // HP threshhold % to increase charge rate - If you find your charge rate bouncing between
        // two numbers, decrease this a bit. This will reduce speed efficiency a bit so only use if needed.
        const float LOW_HP_PERCENT = 95;

        // How much to change charge rate by each adjustment.
        const float ADJUST_STEP = 0.1f;

        //////////////////////////////////
        /////////// End CONFIG ///////////
        //////////////////////////////////

        private long _nextUpdateTicks = 0;
        private IMyUpgradeModule _mainShieldController = null;
        private double _maxRun = 0;
        private double _runMsSum = 0;
        private long _numRun = 0;
        public Program ()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Once | UpdateFrequency.Update10;
        }

        public void Save ()
        {
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            if ( INCLUDE_DEBUG_INFO )
            {
                CheckPerformance ();
            }

            if ( _nextUpdateTicks < DateTime.Now.Ticks )
            {
                _nextUpdateTicks = DateTime.Now.Ticks + TimeSpan.TicksPerSecond * GRID_SCAN_INTERVAL_SECONDS;
                UpdateShieldList ();
            }

            if ( _mainShieldController != null )
            {
                try
                {
                    var percentHp = GetControllerPercent ( _mainShieldController );

                    if ( percentHp.HasValue )
                    {
                        WriteDebug ( $"percentHp={percentHp}" );

                        var chargeRate = _mainShieldController.GetProperty ( "DS-C_ChargeRate" ).As<float>();
                        var currentRate = chargeRate.GetValue (_mainShieldController);

                        WriteDebug ( $"currentRate={currentRate}" );

                        if ( percentHp >= HIGH_HP_PERCENT && currentRate > 20 )
                        {
                            var newRate = Math.Max(20, currentRate - ADJUST_STEP);
                            chargeRate.SetValue ( _mainShieldController, newRate );
                        }

                        if ( percentHp < LOW_HP_PERCENT )
                        {
                            var newRate = Math.Min(95, currentRate + ADJUST_STEP);
                            chargeRate.SetValue ( _mainShieldController, newRate );
                        }
                    }
                } catch (Exception ex )
                {
                    WriteDebug ( ex.Message );
                }
            }
        }
        private void CheckPerformance ()
        {
            if ( Runtime.LastRunTimeMs > _maxRun )
            {
                _maxRun = Runtime.LastRunTimeMs;
            }

            _numRun++;
            _runMsSum += Runtime.LastRunTimeMs;

            WriteDebug ( $"Last run ms={Runtime.LastRunTimeMs}" );
            WriteDebug ( $"Max run ms={_maxRun}" );
            WriteDebug ( $"Avg run ms={_runMsSum / _numRun}" );
        }

        private float? GetControllerPercent ( IMyUpgradeModule module )
        {
            WriteDebug ( $"Checking {module.DisplayNameText}" );

            var api = module.GetValue<Dictionary<string,Delegate>> ( "DefenseSystemsAPI" );
            return api["GetShieldPercent"].DynamicInvoke ( module ) as float?;
        }

        private void UpdateShieldList ()
        {
            var comps = new List<IMyUpgradeModule>();
            GridTerminalSystem.GetBlocksOfType ( comps );

            foreach ( var c in comps.Where ( c => c.IsSameConstructAs ( Me ) ) ) 
            {
                
                var api = c.GetValue<Dictionary<string,Delegate>> ( "DefenseSystemsAPI" );
                if ( api != null )
                {
                    var main = api["IsShieldUp"].DynamicInvoke ( c ) as bool?;

                    if ( main.HasValue && main.Value ) 
                    {
                        _mainShieldController = c;
                        return;
                    }
                }
            }
        }

        private void WriteDebug ( string debugInfo )
        {
            if ( INCLUDE_DEBUG_INFO )
            {
                Echo ( debugInfo );
            }
        }
    }
}
