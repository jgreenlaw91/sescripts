﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using VRage;
using VRage.Game;
using VRage.Game.ModAPI.Ingame;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        const string ASSEMBLER_EXCLUDE_TAG = "NoAutoQueue";

        static Dictionary<string,int> ComponentAmounts = new Dictionary<string, int> ();
        static Dictionary<string,int> ComponentGoals = new Dictionary<string, int> ();
        static List<IMyCargoContainer> Containers;
        static List<IMyAssembler> Assemblers;
        static int CacheAge = 0;

        public Program ()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update100;

            // These are your target stockpile values. Change them to whatever you want.
            ComponentGoals.Add ( "BulletproofGlass", 100 );
            ComponentGoals.Add ( "Canvas", 100 );
            ComponentGoals.Add ( "Computer", 400 );
            ComponentGoals.Add ( "Construction", 2500 );
            ComponentGoals.Add ( "Detector", 40 );
            ComponentGoals.Add ( "Display", 100 );
            ComponentGoals.Add ( "Explosives", 0 );
            ComponentGoals.Add ( "Girder", 100 );
            ComponentGoals.Add ( "GravityGenerator", 0 );
            ComponentGoals.Add ( "InteriorPlate", 2500 );
            ComponentGoals.Add ( "LargeTube", 200 );
            ComponentGoals.Add ( "Medical", 0 );
            ComponentGoals.Add ( "MetalGrid", 75 );
            ComponentGoals.Add ( "Motor", 500 );
            ComponentGoals.Add ( "PowerCell", 240 );
            ComponentGoals.Add ( "RadioCommunication", 50 );
            ComponentGoals.Add ( "Reactor", 0 );
            ComponentGoals.Add ( "SmallTube", 500 );
            ComponentGoals.Add ( "SolarCell", 320 );
            ComponentGoals.Add ( "SteelPlate", 2500 );
            ComponentGoals.Add ( "Superconductor", 0 );
            ComponentGoals.Add ( "Thrust", 0 );

            foreach ( var pair in ComponentGoals )
            {
                ComponentAmounts.Add ( pair.Key, 0 );
            }

            LoadBlocks ();
        }

        private void ResetComponentAmount ()
        {
            foreach ( var pair in ComponentGoals )
            {
                ComponentAmounts[pair.Key] = 0;
            }
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            try
            {
                ResetComponentAmount ();

                // Get cargo container inventory
                foreach ( var container in Containers )
                {
                    var inv = container.GetInventory ();
                    AddInventoryToComponentAmounts ( inv );
                }

                // Add in assembler inventory
                foreach ( var assembler in Assemblers )
                {
                    var inv = assembler.OutputInventory;
                    var queue = new List<MyProductionItem>();

                    assembler.GetQueue ( queue );

                    AddInventoryToComponentAmounts ( inv );
                    AddProductionQueueToComponentAmounts ( queue );
                }

                // Actually add stuff to queues
                foreach ( var component in ComponentAmounts )
                {
                    var valueNeeded = ComponentGoals[component.Key] - component.Value;
                    if ( valueNeeded > 0 )
                    {

                        MyItemType id = new MyItemType("MyObjectBuilder_BlueprintDefinition", component.Key + "Component");

                        foreach ( var assembler in Assemblers )
                        {
                            // TODO: Change this studpid double try catch nonsense into a proper component lookup :(
                            // Find the first assembler that can make the thing. This works best with coop mode
                            try
                            {
                                if ( assembler.CanUseBlueprint ( id ) && assembler.Enabled && !assembler.CooperativeMode  )
                                {
                                    assembler.AddQueueItem ( id, (MyFixedPoint) valueNeeded );
                                    break;
                                }
                            }
                            catch ( Exception )
                            {
                                MyItemType id2 = new MyItemType("MyObjectBuilder_BlueprintDefinition", component.Key);

                                try
                                {
                                    if ( assembler.CanUseBlueprint ( id2 ) && assembler.Enabled && !assembler.CooperativeMode )
                                    {
                                        // Assemblers are stupid and sometimes have a suffix and sometimes don't.
                                        assembler.AddQueueItem ( id2, (MyFixedPoint) valueNeeded );
                                        break;
                                    }
                                }
                                catch ( Exception )
                                {
                                    Echo ( $"{assembler.CustomName} cannot make {id} or {id2}" );
                                }
                            }
                        }
                    }
                }

                // Reload grid blocks around once per minute (if using Update100).
                if ( ++CacheAge >= 360 )
                {
                    CacheAge = 0;
                    LoadBlocks ();
                }
            }
            catch ( Exception ex )
            {
                Echo ( ex.Message );
                Echo ( ex.StackTrace );
                throw;
            }
        }

        private void AddProductionQueueToComponentAmounts ( List<MyProductionItem> queue )
        {
            foreach ( var item in queue )
            {
                string key;

                if ( item.BlueprintId.SubtypeName.EndsWith ( "Component" ) )
                {
                    key = item.BlueprintId.SubtypeName.Substring ( 0, item.BlueprintId.SubtypeName.Length - 9 );
                }
                else
                {
                    key = item.BlueprintId.SubtypeName;
                }


                if ( ComponentGoals.ContainsKey ( key ) )
                {
                    var amt = (int) item.Amount;
                    ComponentAmounts[key] += amt;
                }
            }
        }

        private void AddInventoryToComponentAmounts ( IMyInventory inv )
        {
            foreach ( var component in ComponentGoals )
            {
                MyItemType id = new MyItemType("MyObjectBuilder_Component", component.Key);

                var item = inv.FindItem(id);

                if ( item.HasValue )
                {

                    var amt = (int) item.Value.Amount;
                    ComponentAmounts[component.Key] += amt;

                }
            }
        }

        private void LoadBlocks ()
        {
            Containers = StorageUtils.GetCargoContainersWithTag ( null, GridTerminalSystem, Me );

            Assemblers = new List<IMyAssembler> ();
            var prodBlocks = ProductionUtils.GetProductionBlocksWithTag ( null, GridTerminalSystem, Me );

            foreach ( var pb in prodBlocks )
            {
                if ( pb is IMyAssembler && pb.CustomData != ASSEMBLER_EXCLUDE_TAG )
                {
                    Assemblers.Add ( pb as IMyAssembler );
                }
            }
        }
    }
}
