﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        const string COCKPIT_NAME = "Cockpit";

        static List<IMyInventory> Inventories;
        static int CacheAge = 0;
        static Dictionary<string, float> Treasury = new Dictionary<string, float>();
        static string ItemType;
        static IMyTextPanel Display;
        static IMyTextSurface TextSurface;

        public Program ()
        {
            if ( string.IsNullOrEmpty ( Me.CustomData ) )
            {
                Echo ( "Custom Data must be set to use this script!" );
                return;
            }
            
            Display = GridTerminalSystem.GetBlockWithName ( Me.CustomData ) as IMyTextPanel;

            IMyCockpit cockpit = GridTerminalSystem.GetBlockWithName(COCKPIT_NAME) as IMyCockpit;
            
            int id = -1;

            if ( int.TryParse ( Me.CustomData, out id ) )
            {
                TextSurface = cockpit?.GetSurface ( id );
                TextSurface.ContentType = ContentType.TEXT_AND_IMAGE;
            }
        
            if ( Display != null || TextSurface != null )
            {
                Runtime.UpdateFrequency = UpdateFrequency.Update100 | UpdateFrequency.Once;
                Echo ( "Script ready" );
            }
            else
            {
                Echo ( $"An LCD panel named '{Me.CustomData}' must be on this grid or a cockpit named {COCKPIT_NAME}." );
                return;
            }

            Inventories = StorageUtils.GetAllInventoriesOnGrid ( GridTerminalSystem, Me );
            ItemType = Storage;

            ResetTreasury ();
        }

        public void Save ()
        {
            Storage = ItemType;
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            switch ( argument.ToLower () )
            {
                case "ore":
                    ItemType = "MyObjectBuilder_Ore";
                    break;
                case "ingot":
                    ItemType = "MyObjectBuilder_Ingot";
                    break;
                case "component":
                    ItemType = "MyObjectBuilder_Component";
                    break;
                default:
                    ItemType = ItemType ?? "MyObjectBuilder_Ingot";
                    break;
            }

            ResetTreasury ();

            try
            {
                foreach ( var inv in Inventories )
                {
                    var items = new List<MyInventoryItem>();
                    inv.GetItems ( items );

                    foreach ( var item in items )
                    {
                        if ( ItemType == item.Type.TypeId )
                        {
                            var key = $"{item.Type.SubtypeId} {item.Type.TypeId.Substring(16)}";

                            // Trim 'Component' stuff because it's inconsistent and dumb
                            key = key.Replace ( "Component", "" ).Trim ();

                            if ( Treasury.ContainsKey ( key ) )
                            {
                                Treasury[key] += (float) item.Amount;
                            }
                        }
                    }
                }

                var disp = "";

                foreach (var item in Treasury )
                {
                    disp += $"{item.Key}: {item.Value.ToString("N2")}\n";
                }

                Display?.WriteText ( disp );
                TextSurface?.WriteText ( disp );

            } catch (Exception ex )
            {
                Echo ( $"{ex.Message}\n{ex.StackTrace}" );
                throw;
            }

            // Reload grid blocks around once per minute (if using Update100).
            if ( ++CacheAge >= 360 )
            {
                CacheAge = 0;
                Inventories = StorageUtils.GetAllInventoriesOnGrid ( GridTerminalSystem, Me );
            }
        }

        private void ResetTreasury ()
        {
            Treasury.Clear ();

            if ( ItemType == "MyObjectBuilder_Ingot" )
            {
                Treasury.Add ( "Iron Ingot", 0 );
                Treasury.Add ( "Silicon Ingot", 0 );
                Treasury.Add ( "Magnesium Ingot", 0 );
                Treasury.Add ( "Cobalt Ingot", 0 );
                Treasury.Add ( "Nickel Ingot", 0 );
                Treasury.Add ( "Gold Ingot", 0 );
                Treasury.Add ( "Silver Ingot", 0 );
                Treasury.Add ( "Uranium Ingot", 0 );
                Treasury.Add ( "Platinum Ingot", 0 );
                Treasury.Add ( "Stone Ingot", 0 ); // I think this is gravel
            }

            if ( ItemType == "MyObjectBuilder_Ore" )
            {
                Treasury.Add ( "Ice Ore", 0 ); // Counts as an ore...
                Treasury.Add ( "Iron Ore", 0 );
                Treasury.Add ( "Silicon Ore", 0 );
                Treasury.Add ( "Magnesium Ore", 0 );
                Treasury.Add ( "Cobalt Ore", 0 );
                Treasury.Add ( "Nickel Ore", 0 );
                Treasury.Add ( "Gold Ore", 0 );
                Treasury.Add ( "Silver Ore", 0 );
                Treasury.Add ( "Uranium Ore", 0 );
                Treasury.Add ( "Platinum Ore", 0 );
                Treasury.Add ( "Stone Ore", 0 ); // This too...
            }

            if (ItemType == "MyObjectBuilder_Component" )
            {
                Treasury.Add ( "BulletproofGlass", 0 );
                Treasury.Add ( "Canvas", 0 );
                Treasury.Add ( "Computer", 0 );
                Treasury.Add ( "Construction", 0 );
                Treasury.Add ( "Detector", 0 );
                Treasury.Add ( "Display", 0 );
                Treasury.Add ( "Explosives", 0 );
                Treasury.Add ( "Girder", 0 );
                Treasury.Add ( "GravityGenerator", 0 );
                Treasury.Add ( "InteriorPlate", 0 );
                Treasury.Add ( "LargeTube", 0 );
                Treasury.Add ( "Medical", 0 );
                Treasury.Add ( "MetalGrid", 0 );
                Treasury.Add ( "Motor", 0 );
                Treasury.Add ( "PowerCell", 0 );
                Treasury.Add ( "RadioCommunication", 0 );
                Treasury.Add ( "Reactor", 0 );
                Treasury.Add ( "SmallTube", 0 );
                Treasury.Add ( "SolarCell", 0 );
                Treasury.Add ( "SteelPlate", 0 );
                Treasury.Add ( "Superconductor", 0 );
                Treasury.Add ( "Thrust", 0 );
            }
        }
    }
}
