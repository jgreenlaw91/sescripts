﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        //////////////////////////////////
        ////////// START CONFIG //////////
        //////////////////////////////////

        // How often to scan for new gyros. Higher values are more performance friendly.
        const int GRID_SCAN_INTERVAL_SECONDS = 10;

        // Command to Run (in the Programmable Block) to set the new sensitivity
        const string PAUSE_ADJUST = "Pause";
        const string SAVE_SENSITIVITY_COMMAND = "Save";
        const bool INCLUDE_RUNTIME_INFO = false;

        // Any cockpit or remote control block
        const string REFERENCE_BLOCK_NAME = "Cockpit";

        //////////////////////////////////
        /////////// End CONFIG ///////////
        //////////////////////////////////

        private long _nextUpdateTicks = 0;
        private float? _gyroPowerPerKg = null;
        private IMyShipController _refBlock;
        private bool _shouldAdjust = true;

        private List<IMyGyro> _gyros = new List<IMyGyro>();
        private double _minRun = float.MaxValue;
        private double _maxRun = float.MinValue;


        public Program ()
        {
            if ( Storage != null ) 
            {
                float pwr;

                if ( float.TryParse ( Storage, out pwr ) )
                {
                    _gyroPowerPerKg = pwr;
                }
            }

            Runtime.UpdateFrequency = UpdateFrequency.Update10 | UpdateFrequency.Once;
        }

        public void Save ()
        {
            Storage = _gyroPowerPerKg.Value.ToString ();
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            if ( !string.IsNullOrEmpty(argument) )
            {
                switch ( argument )
                {
                    case SAVE_SENSITIVITY_COMMAND:
                        UpdateSensitivity ();
                        _shouldAdjust = true;
                        break;
                    case PAUSE_ADJUST:
                        _shouldAdjust = false;
                        break;
                }

                return;
            }

            if ( _nextUpdateTicks < DateTime.Now.Ticks && _shouldAdjust )
            {
                _nextUpdateTicks = DateTime.Now.Ticks + TimeSpan.TicksPerSecond * GRID_SCAN_INTERVAL_SECONDS;
                
                RefreshGyros ();

                if ( !_gyroPowerPerKg.HasValue )
                {
                    UpdateSensitivity ();
                }
            }

            if ( _gyroPowerPerKg.HasValue && _shouldAdjust )
            {
                MyShipMass currentMass;

                if ( _refBlock != null )
                {
                    currentMass = _refBlock.CalculateShipMass ();
                    Echo ( $"Calibrating {_gyros.Count} gyroscopes" );
                }
                else
                {
                    Echo ( $"No cockpit/remote control block found with name {REFERENCE_BLOCK_NAME}" );
                    return;
                }

                WriteDebug ( $"Ship mass = {currentMass.BaseMass}" );
                WriteDebug ( $"Physical mass = {currentMass.PhysicalMass}" );
                WriteDebug ( $"Total mass = {currentMass.TotalMass}" );

                foreach ( var gyro in _gyros )
                {
                    WriteDebug ( $"Setting gyro {gyro.DisplayNameText} to power {( currentMass.PhysicalMass * _gyroPowerPerKg.Value ) / _gyros.Count}" );
                   
                    gyro.GyroPower = ( currentMass.PhysicalMass * _gyroPowerPerKg.Value ) / _gyros.Count;
                }
            }

            _minRun = Runtime.LastRunTimeMs < _minRun ? Runtime.LastRunTimeMs : _minRun;
            _maxRun = Runtime.LastRunTimeMs > _maxRun ? Runtime.LastRunTimeMs : _maxRun;

            WriteDebug ( $"Last run ms: {Runtime.LastRunTimeMs}" );
            WriteDebug ( $"Min run ms: {_minRun}" );
            WriteDebug ( $"Max run ms: {_maxRun}" );            
        }

        private void WriteDebug ( string debugInfo )
        {
            if ( INCLUDE_RUNTIME_INFO )
            {
                Echo ( debugInfo );
            }
        }

        private void UpdateSensitivity ()
        {
            float powerTotal = 0;

            foreach ( var gyro in _gyros )
            {
                powerTotal += gyro.GyroPower;
            }

            if ( _refBlock != null )
            {
                _gyroPowerPerKg = powerTotal / _refBlock.CalculateShipMass ().PhysicalMass;
            }
            else
            {
                Echo ( $"No cockpit/remote control block found with name {REFERENCE_BLOCK_NAME}" );
                return;
            }

            Echo ( $"New power/Kg = {_gyroPowerPerKg}" );
        }

        private void RefreshGyros ()
        {
            GridTerminalSystem.GetBlocksOfType ( _gyros );
            _gyros = _gyros.Where ( g => g.IsSameConstructAs ( Me ) ).ToList ();
            _refBlock = (IMyShipController) GridTerminalSystem.GetBlockWithName ( REFERENCE_BLOCK_NAME );
        }
    }
}
