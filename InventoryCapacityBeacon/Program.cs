﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        static List<IMyInventory> Inventories;
        static IMyBeacon Beacon;
        const string BEACON_TAG = "CapacityDisplay";
        const string FILTER_TAG = "IgnoreCapacity";
        string cockpitName = "Cockpit";
        IMyTextSurface TextSurface;

        public Program ()
        {
            Inventories = StorageUtils.GetAllInventoriesOnGrid ( GridTerminalSystem, Me, FILTER_TAG );

            List<IMyBeacon> blocks = new List<IMyBeacon>();
            GridTerminalSystem.GetBlocksOfType ( blocks, block => block.IsSameConstructAs ( Me ) 
                                                 && block.CustomData == BEACON_TAG );
            Beacon = blocks.FirstOrDefault ();
            var cs = GridTerminalSystem.GetBlockWithName ( cockpitName ) as IMyCockpit;

            if ( cs != null && cs.SurfaceCount > 0 )
            {
                TextSurface = ( cs.GetSurface ( 0 ) );
            }

            if ( Beacon != null )
            {
                Beacon.Radius = 5;
            }

            if ( TextSurface != null )
            {
                TextSurface.ContentType = ContentType.TEXT_AND_IMAGE;
            }

            if (Beacon == null && TextSurface == null)
            {
                Echo ( $"This script requires either a beacon with CustomData of {BEACON_TAG} or a cockpit named {cockpitName}." );
                return;
            }

            Runtime.UpdateFrequency = UpdateFrequency.Update100 | UpdateFrequency.Once;
        }


        public void Main ( string argument, UpdateType updateSource )
        {
            var capacity = StorageUtils.GetTotalFreeSpacePercent ( Inventories ) * 100;
            
            var disp = $"Capacity: {capacity.ToString("N2")}";

            if ( Beacon != null )
            {
                Beacon.HudText = disp;
            }

            if (TextSurface != null )
            {
                TextSurface.WriteText ( disp );
            }
        }
    }
}
