﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {   
        /***************************************************************
        **************************** CONFIG ****************************
        ****************************************************************/
        const float START_DRILLING_THRESHOLD = 0.80f;
        const float START_PROCESSING_THRESHOLD = 0.15f;
        const float PISTON_TARGET_VELOCITY = 0.0075f;
        const string AUTODRILL_GROUP_NAME = "AutoDrill";

        static List<IMyInventory> CargoInventories;
        static List<IMyShipDrill> Drills = new List<IMyShipDrill> ();
        static List<IMyPistonBase> Pistons = new List<IMyPistonBase> ();
        static List<IMyRefinery> Refineries = new List<IMyRefinery>();

        static IMyMotorStator Rotor;
        static ScriptState CurrentState = ScriptState.Stopped;

        public Program ()
        {
            bool loadSuccess = true;

            var group = GridTerminalSystem.GetBlockGroupWithName ( AUTODRILL_GROUP_NAME );

            CargoInventories = GetInventoriesFromCollection ( group );
            Rotor = GetRotorFromCollection ( group );

            group.GetBlocksOfType ( Pistons );
            group.GetBlocksOfType ( Drills );
            group.GetBlocksOfType ( Refineries );
            
            if (CargoInventories.Count == 0 )
            {
                Echo ( $"No containers were found. Make sure at least one starage container exists in a group named {AUTODRILL_GROUP_NAME}." );
                loadSuccess = false;
            }

            if ( Drills.Count == 0 )
            {
                Echo ( $"No drills were found. Make sure at least one drill exists in a group named {AUTODRILL_GROUP_NAME}." );
                loadSuccess = false;
            }

            if ( Pistons.Count == 0 )
            {
                Echo ( $"No pistons were found. Make sure at least one piston exists in a group named {AUTODRILL_GROUP_NAME}." );
                loadSuccess = false;
            }

            if ( Rotor == null )
            {
                Echo ( $"No rotor was found. Make sure one rotor exists in a group named {AUTODRILL_GROUP_NAME}." );
                loadSuccess = false;
            }

            if ( loadSuccess )
            {
                Runtime.UpdateFrequency = UpdateFrequency.Update100 | UpdateFrequency.Once;
                if ( !string.IsNullOrEmpty ( Storage ) )
                {
                    CurrentState = (ScriptState) Enum.Parse ( typeof ( ScriptState ), Storage );
                }
            }
        }

        private IMyMotorStator GetRotorFromCollection ( IMyBlockGroup group )
        {
            var list = new List<IMyMotorStator> ();
            group.GetBlocksOfType ( list );
            return list.FirstOrDefault ();
        }

        private List<IMyInventory> GetInventoriesFromCollection ( IMyBlockGroup group )
        {
            var inventories = new List<IMyInventory> ();
            var blocks = new List<IMyCargoContainer> ();
            group.GetBlocksOfType ( blocks );

            foreach ( var block in blocks ) 
            {
                var invCount = block.InventoryCount;

                for ( int i = 0; i < invCount; i++ )
                {
                    inventories.Add ( block.GetInventory ( i ) );
                }
            }

            return inventories;
        }

        public void Save ()
        {
            Storage = CurrentState.ToString();
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            foreach ( var refinery in Refineries )
            {
                foreach ( var inv in CargoInventories )
                {
                    inv.TransferItemTo ( refinery.InputInventory, 0, stackIfPossible: true );
                }
            }

            if (string.Equals(argument, "start", StringComparison.InvariantCultureIgnoreCase ) )
            {
                CurrentState = ScriptState.Processing;
            }

            if ( string.Equals ( argument, "stop", StringComparison.InvariantCultureIgnoreCase ) )
            {
                CurrentState = ScriptState.Stopped;
                StopDrilling ();
                return;
            }

            var currentCapacity = StorageUtils.GetTotalFreeSpacePercent ( CargoInventories );
            Echo ( $"currentCapacity: {currentCapacity:0.###}" );

            if (currentCapacity > START_DRILLING_THRESHOLD && CurrentState != ScriptState.Stopped )
            {
                BeginDrilling ();
            }

            if ( currentCapacity < START_PROCESSING_THRESHOLD || CurrentState == ScriptState.Stopped )
            {
                StopDrilling ();
            }
        }

        private void BeginDrilling ()
        {
            CurrentState = ScriptState.Drilling;

            foreach ( var drill in Drills )
            {
                drill.SetValueBool ( "OnOff", true );
            }

            float singlePistonVelocity = PISTON_TARGET_VELOCITY / Pistons.Count;

            foreach ( var piston in Pistons )
            {
                piston.Velocity = singlePistonVelocity;
                piston.SetValueBool ( "OnOff", true );

                // We want ShareInertiaTensor on everything except the first piston in the chain.
                // This will let us have far larger piston chains, but if all pistons are sharing
                // with the primary grid, it bugs out.
                if ( !piston.IsSameConstructAs ( Me ) )
                {
                    piston.SetValueBool ( "ShareInertiaTensor", true );
                }
            }

            Rotor.SetValueBool ( "OnOff", true );
        }

        private void StopDrilling ()
        {
            CurrentState = CurrentState == ScriptState.Stopped ? ScriptState.Stopped : ScriptState.Processing;

            foreach ( var drill in Drills )
            {
                drill.SetValueBool ( "OnOff", false );
            }

            foreach ( var piston in Pistons )
            {
                piston.SetValueBool ( "OnOff", false );
            }

            Rotor.SetValueBool ( "OnOff", false );
        }
        

        enum ScriptState
        {
            Drilling,
            Processing,
            Stopped
        }
    }
}