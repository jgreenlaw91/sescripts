﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // SETTINGS
        const string    GRID_PREFIX = "MyGrid"; // All terminal blocks on the same grid will have this prefix
        const bool      REPLACE_SPACE_WITH_DASH = true; // Replace all spaces with a dash (-) characer?
        const bool      ADD_NUMERIC_SUFFIX = true; // Every block will have a numberic suffix, eg "MyGrid-Battery-01"

        private int _numRuns = 0;

        public Program ()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update100;
        }

        public void Main ( string argument, UpdateType updateSource )
        {
            // We REALLY don't want to run this often, otherwise simspeed will suffer. It can be called on demand, if needed.
            // This is overidden if called manually
            if ( ( updateSource == UpdateType.Update100
                || updateSource == UpdateType.Update10
                || updateSource == UpdateType.Update1 )
                && _numRuns++ % 10 != 0 )
            {
                return;
            }

            Echo ( "Getting Blocks" );
            var blocks = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocks(blocks);

            foreach ( var block in blocks.Where( b => b.IsSameConstructAs ( Me ) ) )
            {
                var baseName = string.IsNullOrEmpty(block.CustomData) ? block.DefinitionDisplayNameText : block.CustomData;
                var name = GetBlockName ( baseName, block.NumberInGrid );

                block.CustomName = name;
            }
        }

        private string GetBlockName ( string baseName, int numInSeries )
        {
            var blockNameSan = REPLACE_SPACE_WITH_DASH ? baseName.Replace(' ', '-') : baseName;
            var name = $"{GRID_PREFIX}-{blockNameSan}";

            if ( ADD_NUMERIC_SUFFIX )
            {
                name += $"-{numInSeries.ToString ().PadLeft ( 2, '0' )}";
            }

            return name;
        }
    }
}
