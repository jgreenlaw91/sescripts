﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        static class ProductionUtils
        {
            /// <summary>
            /// Get a list of all IMyProductionBlock attached to a grid. This only takes into account the current
            /// grid ignoring, for example, assemblers attached via connectors. It WILL pick up anything
            /// attached with merge block, though.
            /// </summary>
            /// <param name="tag">
            ///     Filter out any production blocks that do not have this string in their "Custom Data" entry.
            ///     Leave empty to get all containers, regardless of tags.
            /// </param>
            /// <param name="grid">Always pass in the GridTerminalSystem</param>
            /// <param name="program">Pass in "Me"</param>
            /// <returns>Returns all matching IMyCargoContainers on the same grid.</returns>
            public static List<IMyProductionBlock> GetProductionBlocksWithTag ( string tag, IMyGridTerminalSystem grid, IMyProgrammableBlock program )
            {
                List<IMyProductionBlock> blocks = new List<IMyProductionBlock>();

                grid.GetBlocksOfType ( blocks, block => block.IsSameConstructAs ( program )
                      && ( string.IsNullOrEmpty ( tag ) || block.CustomData == tag ) );

                return blocks;
            }
        }
    }
}
