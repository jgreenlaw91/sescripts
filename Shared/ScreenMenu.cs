﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IngameScript
{
    public class ScreenMenu
    {
        public string DisplayText { get; set; }
        public List<ScreenMenu> Menus { get; set; } = new List<ScreenMenu> ();
        public List<ScreenMenuItem> MenuItems { get; set; } = new List<ScreenMenuItem> ();
        public bool ShowCursor { get; set; } = true;

        public bool IsLeaf => _submenuIndex == null;

        private int _cursorPosition = 0;
        private int? _submenuIndex = null;
        private List<string> _displayStrings = new List<string> ();

        public ScreenMenu (string displayText )
        {
            DisplayText = displayText;
        }

        public void Up ()
        {

            if ( _submenuIndex == null )
            {
                if ( _cursorPosition > 0 )
                {
                    _cursorPosition--;
                }
            }
            else
            {
                Menus[_submenuIndex.Value].Up ();
            }
        }

        public void Down ()
        {
            if ( IsLeaf )
            {
                if ( _cursorPosition < Menus.Count + MenuItems.Count - 1 )
                {
                    _cursorPosition++;
                }
            }
            else
            {
                Menus[_submenuIndex.Value].Down ();
            }
        }

        public void Enter ()
        {
            if ( IsLeaf )
            {
                if ( _cursorPosition < Menus.Count )
                {
                    _submenuIndex = _cursorPosition;
                } else
                {
                    var actionIndex = _cursorPosition - Menus.Count;
                    MenuItems[actionIndex].Action ();
                }
            }
            else
            {
                Menus[_submenuIndex.Value].Enter ();
            }
        }

        public void Back ()
        {
            if (!IsLeaf && Menus.All( m => m.IsLeaf ) )
            {
                _submenuIndex = null;
            }
        }

        public void RunCommandString ( string command )
        {
            if ( IsLeaf )
            {
                throw new Exception ("NOT DONE");
            }
            else
            {
                Menus[_submenuIndex.Value].RunCommandString ( command );
            }
        }

        public string GetDisplayString (int charWidth, int numLines)
        {
            if ( !IsLeaf )
            {
                return Menus[_submenuIndex.Value].GetDisplayString ( charWidth, numLines );
            }
            else
            {
                if ( _displayStrings.Count == 0 ) 
                {
                    RefreshDisplayStrings ();
                }

                var startIndex = _cursorPosition - numLines / 2 < 0 ?  0 : _cursorPosition - numLines / 2;
                var endIndex = startIndex + numLines >  _displayStrings.Count ? _displayStrings.Count : startIndex + numLines;

                var retString = new StringBuilder();

                for ( int i = startIndex; i < endIndex; i++ )
                {
                    var line = _displayStrings[i];
                    
                    if ( line.Length > charWidth - 2 )
                    {
                        line = line.Substring ( 0, charWidth - 2 );
                    }

                    if ( i == _cursorPosition && ShowCursor ) 
                    {
                        var lineArr = line.ToCharArray ();
                        lineArr[0] = '>';
                        line = new string(lineArr);
                    }

                    retString.AppendLine ( line );
                }

                return retString.ToString ();
            }
        }

        public void RefreshDisplayStrings ()
        {
            _displayStrings.Clear ();

            foreach ( var menu in Menus )
            {
                _displayStrings.Add ( " /" + menu.DisplayText );
            }

            foreach ( var item in MenuItems )
            {
                _displayStrings.Add ( "  " + item.DisplayText );
            }
        }

        /// <summary>
        /// Get the help text for an associated item.
        /// </summary>
        /// <returns>Returns the help text assosiated with an item or null it isn't available or a submenu is selected</returns>
        public string GetHelpText ()
        {
            if ( IsLeaf )
            {
                if ( _cursorPosition >= Menus.Count )
                {
                    var helpIndex = _cursorPosition - Menus.Count;
                    return MenuItems[helpIndex].HelpText;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return Menus[_cursorPosition].GetHelpText ();
            }
        }
    }
}
