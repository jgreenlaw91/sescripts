﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        static class StorageUtils
        {

            /// <summary>
            /// Get a list of all IMyCargoContainers attached to a grid. This only takes into account the current
            /// grid ignoring, for example, cargo containers attached via connectors. It WILL pick up anything
            /// attached with merge block, though.
            /// </summary>
            /// <param name="tag">
            ///     Filter out any continers that do not have this string in their "Custom Data" entry.
            ///     Leave empty to get all containers, regardless of tags.
            /// </param>
            /// <param name="grid">Always pass in the GridTerminalSystem</param>
            /// <param name="program">Pass in "Me" prom the main program</param>
            /// <returns>Returns all matching IMyCargoContainers on the same grid.</returns>
            public static List<IMyCargoContainer> GetCargoContainersWithTag ( string tag, IMyGridTerminalSystem grid, IMyProgrammableBlock program )
            {
                List<IMyCargoContainer> blocks = new List<IMyCargoContainer>();

                grid.GetBlocksOfType ( blocks, block => block.IsSameConstructAs ( program )
                      && ( string.IsNullOrEmpty ( tag ) || block.CustomData == tag ) );

                return blocks;
            }

            public static List<IMyInventory> GetAllInventoriesOnGrid ( IMyGridTerminalSystem grid, IMyProgrammableBlock program, 
                string customDataToIgnore = null, string customDataToInclude = null, bool onlyIncludeSameGrid = true )
            {
                var blocks = new List<IMyTerminalBlock> ();
                var inventories = new List<IMyInventory> ();

                grid.GetBlocksOfType ( blocks, block =>
                                                 ( !onlyIncludeSameGrid || block.IsSameConstructAs ( program ) )
                                                 && ( customDataToIgnore == null || block.CustomData != customDataToIgnore )
                                                 && ( customDataToInclude == null || block.CustomData == customDataToInclude ) );

                foreach (var block in blocks )
                {
                    var invCount = block.InventoryCount;

                    for (int i = 0; i < invCount; i++ )
                    {
                        inventories.Add ( block.GetInventory ( i ) );
                    }
                }

                return inventories;
            }

            /// <summary>
            /// Calculates the percent of free space in a set of IMyCargoContainer blocks.
            /// </summary>
            /// <param name="cargoContainers">
            ///     Containers to check. This should be set as a static variable in your main script and reused,
            ///     since fetching a block list is expensive. If needed add a manual or periodic (once per minute 
            ///     or less) call to GetCargoContainersWithTag and store the results.
            /// </param>
            /// <returns></returns>
            public static double GetTotalFreeSpacePercent ( List<IMyInventory> inventories )
            {
                float totalMaxVolume = 0;
                float totalCurrentVolume = 0;

                foreach ( var inv in inventories )
                {
                    totalMaxVolume += (float) inv.MaxVolume;
                    totalCurrentVolume += (float) inv.CurrentVolume;

                }

                return 1 - ( totalCurrentVolume / totalMaxVolume );
            }
        }
    }
}
