﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngameScript
{
    public class ScreenMenuItem
    {
        public string DisplayText { get; }
        public string HelpText { get; }
        public Action Action { get; }

        public ScreenMenuItem ( string displayText, Action action, string helpText = null )
        {
            DisplayText = displayText;
            Action = action;
            HelpText = helpText;
        }
    }
}
