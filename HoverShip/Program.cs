﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        const string REMOTE_CONTROL_NAME = "HoverControl";
        const string THRUSTER_GROUP_NAME = "HoverThrusters";
        const int TARGET_ALTITUDE = 12;
        const float MAX_SPEED = 1f;
        const string COCKPIT_NAME = "Cockpit";

        private IMyRemoteControl _remoteControl = null;
        private List<IMyThrust> _thrusters = new List<IMyThrust>();
        private IMyTextSurface _statusDisplay = null;
        private List<IMyLandingGear> _landingGear = new List<IMyLandingGear>();

        private float _lastThrustStr = 0f;
        private double _lastAltitude = 0f;
        private bool _landingMode = true;
        private bool _manualMode = false;

        public Program ()
        {
            // block declarations 
            string ERR_TXT = "";
            List<IMyTerminalBlock> l0 = new List<IMyTerminalBlock>();

            GridTerminalSystem.GetBlocksOfType ( _landingGear );

            GridTerminalSystem.GetBlocksOfType<IMyRemoteControl> ( l0, filterThis );

            if ( l0.Count == 0 )
            {
                ERR_TXT += "No Remote Control blocks found\n";
            }
            else
            {
                for ( int i = 0; i < l0.Count; i++ )
                {
                    if ( l0[i].CustomName == REMOTE_CONTROL_NAME )
                    {
                        _remoteControl = (IMyRemoteControl) l0[i];
                        break;
                    }
                }

                if ( _remoteControl == null )
                {
                    ERR_TXT += $"No Remote Control block named \"{REMOTE_CONTROL_NAME}\"\n";
                }
            }

            // Get thrusters
            var group = GridTerminalSystem.GetBlockGroupWithName ( THRUSTER_GROUP_NAME );

            if (group != null)
            {
                group.GetBlocksOfType ( _thrusters );

                if ( _thrusters.Count == 0 )
                {
                    ERR_TXT += $"No thrusters are in \"{THRUSTER_GROUP_NAME}\"\n";
                }
            }
            else
            {
                ERR_TXT += $"No group named \"{THRUSTER_GROUP_NAME}\"\n";
            }

            var cs = GridTerminalSystem.GetBlockWithName ( COCKPIT_NAME ) as IMyCockpit;

            if ( cs != null && cs.SurfaceCount > 0 )
            {
                _statusDisplay = ( cs.GetSurface ( 0 ) );
                _statusDisplay.ContentType = ContentType.TEXT_AND_IMAGE;
            }

            // display errors 
            if ( ERR_TXT != "" )
            {
                Echo ( "Script Errors:\n" + ERR_TXT + "(make sure block ownership is set correctly)" );
                return;
            }
            else
            {
                Echo ( "Startup successful." );
                Runtime.UpdateFrequency = UpdateFrequency.Update10;
            }
        }

        void Main ( string argument )
        {
            if ( !string.IsNullOrEmpty ( argument ) )
            {
                switch ( argument )
                {
                    case "ToggleLanding":
                        _landingMode = !_landingMode;

                        if ( !_landingMode )
                        {
                            foreach (var lg in _landingGear )
                            {
                                lg.Unlock ();
                            }
                        }

                        break;
                    case "ToggleManual":
                        _manualMode = !_manualMode;
                        break;
                }
            }

            if (_statusDisplay != null)
            {
                _statusDisplay.WriteText ( $"Landing: {_landingMode}\nManual Control: {_manualMode}" );
            }

            if ( _manualMode )
            {
                foreach (var thrust in _thrusters )
                {
                    thrust.ThrustOverridePercentage = 0f;
                }
            }
            else
            {
                var ta = _landingMode ? 0 : TARGET_ALTITUDE;
                FlightLogic ( ta );
            }
        }

        private void FlightLogic (int targetAltitude)
        {
            double alt = 0;
            _remoteControl.TryGetPlanetElevation ( MyPlanetElevation.Surface, out alt );
            var rate = alt -_lastAltitude;
            var distToTarget = Math.Abs(targetAltitude - alt);
            var targetRate = 0f;
            float thrustStr = _lastThrustStr;
            var failsafe = false;
            var anyLock = _landingGear.Any(lg => lg.LockMode == LandingGearMode.Locked || lg.LockMode == LandingGearMode.ReadyToLock);

            if ( anyLock && _landingMode )
            {
                foreach ( var thrust in _thrusters )
                {
                    thrust.ThrustOverridePercentage = 0f;
                }

                return;
            }

            if ( distToTarget > targetAltitude * 0.25f )
            {
                if ( alt < targetAltitude )
                {
                    targetRate = MAX_SPEED;
                }
                else
                {
                    targetRate = -MAX_SPEED;
                }

                if ( rate > targetRate )
                {
                    thrustStr -= .01f;

                    if ( thrustStr <= 0 )
                    {
                        thrustStr = 0.001f;
                    }
                }
                else
                {
                    thrustStr += .01f;
                }
            }
            else
            {
                thrustStr = 0f;
            }

            // failsafe
            if ( rate < -1 || alt < targetAltitude * 0.7f )
            {
                thrustStr = 100f;
                failsafe = true;
            }

            foreach ( var thrust in _thrusters )
            {
                thrust.ThrustOverridePercentage = thrustStr;
            }

            Echo ( $"Alt:{alt}\n_lastAltitude:{_lastAltitude}\nrate:{rate}\ndistToTarget: {distToTarget}\n_thrustStr: {thrustStr}\ntargetRate: {targetRate}" );
            _lastAltitude = alt;

            if ( !failsafe && thrustStr != 0 )
            {
                // only save the normal thrustStr
                _lastThrustStr = thrustStr;
            }
        }

        bool filterThis ( IMyTerminalBlock block ) { return block.CubeGrid == Me.CubeGrid; }
    }

}